﻿/*using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player2 : MovingObjects {
	
	public Text healthText;
	public AudioClip movementSound1;
	public AudioClip movementSound2;
	public AudioClip chopSound1;
	public AudioClip chopSound2;
	public AudioClip fruitSound1;
	public AudioClip fruitSound2;
	public AudioClip sodaSound1;
	public AudioClip sodaSound2;
	public AudioClip chipSound1;
	public AudioClip chipSound2;
	
	
	
	private Animator animator;
	private int playerHealth;
	private int attackPower = 2;
	private int healthPerFruit = 8;
	private int healthPerSoda = 17;
	private int healthPerChips = 15;
	private int secondsUntilNextLevel = 1;
	
	protected override void Start(){
		base.Start ();
		animator = GetComponent<Animator> ();
		playerHealth = GameController.Instance.playerCurrentHealth;
		healthText.text = "Health: " + playerHealth;
	}
	
	private void OnDisable(){
		GameController.Instance.playerCurrentHealth = playerHealth;
	}
	
	void Update () {
		
		if (!GameController.Instance.isPlayerTurn) {
			return;
		}
		CheckIfGameOver ();
		int xAxis = 0;
		int yAxis = 0;
		
		xAxis = (int)Input.GetAxisRaw ("Horizontal");
		yAxis = (int)Input.GetAxisRaw ("Vertical");
		
		if (xAxis != 0) {
			yAxis = 0;
		}
		if (xAxis != 0 || yAxis != 0) {
			playerHealth--;
			healthText.text = "Health: " + playerHealth;
			SoundController.Instance.PlaySingle(movementSound1, movementSound2);
			Move<Wall>(xAxis, yAxis);
			GameController.Instance.isPlayerTurn = false;
		}
	}
	
	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith){
		if (objectPlayerCollidedWith.tag == "Exit") {
			Invoke("LoadNewLevel", secondsUntilNextLevel);
			enabled = false;
		}
		else if (objectPlayerCollidedWith.tag == "Fruit") {
			SoundController.Instance.PlaySingle(fruitSound1, fruitSound2);
			playerHealth +=  healthPerFruit;
			healthText.text = "+" + healthPerFruit + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
		}
		else if (objectPlayerCollidedWith.tag == "Soda") {
			SoundController.Instance.PlaySingle(sodaSound1, sodaSound2);
			playerHealth +=  healthPerSoda;
			healthText.text = "+" + healthPerSoda + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
		}
		else if (objectPlayerCollidedWith.tag == "Chips") {
			SoundController.Instance.PlaySingle(chipSound1, chipSound2);
			playerHealth +=  healthPerChips;
			healthText.text = "+" + healthPerChips + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive(false);
		}
	}
	private void LoadNewLevel (){
		Application.LoadLevel (Application.loadedLevel);
	}
	

	public void TakeDamage(int damageRecieved){
		playerHealth -= damageRecieved * 2;
		healthText.text = "-" + damageRecieved * 2 + " Health\n" + "Health: " + playerHealth;
		animator.SetTrigger ("playerHurt");
	}
	private void CheckIfGameOver(){
		
		if (playerHealth <= 0) {
			GameController.Instance.GameOver();
		}
	}
}
*/
